//
//  LetterProgressCell.swift
//  AlefAdTav
//
//  Created by Sapir Kalbin on 24/07/2019.
//  Copyright © 2019 Sapir Kalbin. All rights reserved.
//

import UIKit

class LetterProgressCell: UICollectionViewCell {
    @IBOutlet weak var letterImage: UIImageView!
    @IBOutlet weak var progress: UIProgressView!
    var name: String = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
