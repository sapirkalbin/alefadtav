//
//  AppHelper.swift
//  AlefAdTav
//
//  Created by Sapir Kalbin on 23/07/2019.
//  Copyright © 2019 Sapir Kalbin. All rights reserved.
//

import Foundation

final class AppHelper {
    public static let StoryBoardName: String = "Main";
    public static let MainMenuID: String = "MainMenu";

}
